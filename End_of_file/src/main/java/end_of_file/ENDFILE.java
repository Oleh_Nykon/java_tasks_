package end_of_file;
import java.util.Scanner;

public class ENDFILE {
    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args){

        Scanner number = new Scanner(System.in);
        int j;
        j= number.nextInt();

        for (int i=1; i<j; i++) {

            Scanner str = new Scanner(System.in);
            String string = str.nextLine();

            System.out.println("String "+ i +": " + string);
        }
    }

}
